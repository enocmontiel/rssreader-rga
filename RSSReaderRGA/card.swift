//
//  card.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/20/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//

import Foundation

class Card {
    var title: String?
    var description: String?
    var link: String?
    var image: String?
    
    init(title: String?,
         description: String?,
         link: String?,
         image: String?) {
        self.title = title
        self.description = description
        self.link = link
        self.image = image
    }
}
