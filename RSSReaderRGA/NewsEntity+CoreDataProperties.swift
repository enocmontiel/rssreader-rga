//
//  NewsEntity+CoreDataProperties.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/24/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension NewsEntity {

    @NSManaged var descrip: String?
    @NSManaged var image: String?
    @NSManaged var link: String?
    @NSManaged var title: String?

}
