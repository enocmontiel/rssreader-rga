//
//  Config+CoreDataProperties.swift
//  RSSReaderRGA
//
//  Created by emontiel on 6/24/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Config {

    @NSManaged var url: String?

}
