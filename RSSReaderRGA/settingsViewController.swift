//
//  settingsViewController.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/24/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//

import UIKit
import CoreData
import SwiftSpinner

class settingsViewController: UIViewController {

    @IBOutlet weak var feedSource: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Settings"
        
        self.feedSource.textColor = UIColor.whiteColor()
        
        let fetchRequest = NSFetchRequest()
        let entityDescription = NSEntityDescription.entityForName("Config", inManagedObjectContext: DataController().managedObjectContext)
        fetchRequest.entity = entityDescription
        
        do {
            let result = try DataController().managedObjectContext.executeFetchRequest(fetchRequest)
            let config = result[0] as! NSManagedObject
            self.feedSource.text = config.valueForKey("url")  as? String
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func saveConfig(sender: AnyObject) {
        SwiftSpinner.show("Updating Values")
        

        let fetchRequest = NSFetchRequest()
        let entityDescription = NSEntityDescription.entityForName("Config", inManagedObjectContext: DataController().managedObjectContext)
        fetchRequest.entity = entityDescription
        
        do {
            let result = try DataController().managedObjectContext.executeFetchRequest(fetchRequest)
            
            if (result.count > 0) {
                let configData = result[0] as! NSManagedObject
                
                configData.setValue(self.feedSource.text, forKey: "url")
                do {
                    try configData.managedObjectContext?.save()
                    SwiftSpinner.hide()
                } catch {
                    let saveError = error as NSError
                    print(saveError)
                }
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }

    }

}
