//
//  itemCell.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/20/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//

import UIKit
import Kingfisher

class itemCell: UITableViewCell {
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var previewText: UILabel!
    @IBOutlet weak var coverImage: UIImageView!

    func setCard(card: Card){
        self.title?.text = card.title
        self.previewText?.text = card.description

        self.coverImage.kf_setImageWithURL(NSURL(string: card.image!)!, placeholderImage: UIImage(named:"placeholder")!)
    }
}
