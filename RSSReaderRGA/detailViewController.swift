//
//  detailViewController.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/21/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//

import UIKit
import SwiftSpinner

class detailViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webViewForDetail: UIWebView!
    
    var card: Card?
    
    override func viewDidLoad() {
            SwiftSpinner.show("Loading Detail")
            if let c = self.card {
            self.navigationItem.title = c.title
            self.webViewForDetail.delegate = self
            let requestURL = NSURL(string:c.link!)
            let request = NSURLRequest(URL: requestURL!)
            self.webViewForDetail.loadRequest(request)
        }
    }
    
   
    @IBAction func openInBrowser(sender: AnyObject) {
        if let c = self.card {
            let url = NSURL(string: c.link!)!
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func webViewDidFinishLoad(webView : UIWebView) {
        SwiftSpinner.hide()
    }
    
}
