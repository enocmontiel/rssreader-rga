//
//  homeController.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/19/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//

import UIKit
import SWXMLHash
import SwiftSpinner
import DGElasticPullToRefresh
import CoreData

class homeController: UITableViewController, UISearchBarDelegate {
    
    var cards: [Card]?
    let moc = DataController().managedObjectContext
    let cardsFetch = NSFetchRequest(entityName: "NewsEntity")
    var preventAnimation = Set<NSIndexPath>()
    var searchActive = false
    var filtered:[Card]?
    var cardsCoreData: [Card]?
    var barHidde = true
    let logo = UIImage(named: "rgaLogoTitle")
    lazy   var searchBar:UISearchBar = UISearchBar(frame: CGRectMake(0, 0, 200, 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        searchBar.placeholder = "Search in news"
        
        let launchedBefore = NSUserDefaults.standardUserDefaults().boolForKey("launchedBefore")
        if !launchedBefore  {
            
            let moc = DataController().managedObjectContext
            let entity = NSEntityDescription.insertNewObjectForEntityForName("Config", inManagedObjectContext: moc) as! Config
            
            entity.setValue("http://feeds.bbci.co.uk/news/rss.xml", forKey: "url")
            
            do {
                try moc.save()
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }

            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "launchedBefore")
        }
        
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView

        
        _ = NSTimer.scheduledTimerWithTimeInterval(120.0, target: self, selector: #selector(homeController.loadParser), userInfo: nil, repeats: true)

        navigationController!.navigationBar.barTintColor = UIColor.blackColor()
        navigationController!.navigationBar.tintColor = UIColor.whiteColor();
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        navigationController!.navigationBar.translucent = false
        cards = [Card]()
        cardsCoreData = [Card]()
        loadParser()
        
        let loadingView = DGElasticPullToRefreshLoadingViewCircle()
        loadingView.tintColor = UIColor.whiteColor()
        tableView.dg_addPullToRefreshWithActionHandler({ [weak self] () -> Void in
            self!.loadParser()
            self!.tableView.dg_stopLoading()
            }, loadingView: loadingView)
        tableView.dg_setPullToRefreshFillColor(UIColor.blackColor())
        tableView.dg_setPullToRefreshBackgroundColor(tableView.backgroundColor!)
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = cardsCoreData!.filter({ (Card) -> Bool in
            let tmp: NSString = Card.title!
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        
        if(filtered!.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchActive) {
            if filtered?.count == 0{
                return cardsCoreData!.count
            } else {
                return self.filtered!.count
            }
        } else {
            return cardsCoreData!.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! itemCell
        
        if(searchActive){
            cell.setCard((filtered?[indexPath.row])!)
        } else {
            cell.setCard((cardsCoreData?[indexPath.row])!)
        }

        return cell
    }
    
    func loadParser(){
        SwiftSpinner.show("Loading News")
        dataHelper.getCards({ (result: [Card]) in
            self.cards = result
            dispatch_async(dispatch_get_main_queue(),{ ()->() in
                do {
                    var fetchCardsArry = [Card]()
                    let results =
                        try self.moc.executeFetchRequest(self.cardsFetch)
                    for result in results as! [NewsEntity] {
                        let card = Card(title: result.title, description: result.descrip, link: result.link, image:result.image)
                        fetchCardsArry.append(card)
                    }
                    self.cardsCoreData = fetchCardsArry
                } catch let error as NSError {
                    print("Could not fetch \(error), \(error.userInfo)")
                }
                
                self.tableView.reloadData()
                SwiftSpinner.hide()
            })
        } )
        
    }
    
    @IBAction func searchAction(sender: AnyObject) {
        if(!barHidde){
            searchActive = false;
            let imageView = UIImageView(image:logo)
            self.navigationItem.titleView = imageView
            barHidde = true;
        } else {
            self.navigationItem.titleView = searchBar
            barHidde = false;
        }
        
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

        if !preventAnimation.contains(indexPath) {
            cell.alpha = 0
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, 0, 80, 0)
            cell.layer.transform = rotationTransform
            preventAnimation.insert(indexPath)
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                cell.alpha = 1
                cell.layer.transform = CATransform3DIdentity
            })
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "detailsSegue" {
            let detailsViewController = segue.destinationViewController as? detailViewController
            guard let cell = sender as? itemCell,
                let indexPath = tableView.indexPathForCell(cell) else {
                    return
            }
            detailsViewController?.card = cardsCoreData?[indexPath.row]
        }
    }

}