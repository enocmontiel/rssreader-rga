//
//  dataHelper.swift
//  RSSReaderRGA
//
//  Created by Enoc Montiel on 6/20/16.
//  Copyright © 2016 Enoc Montiel. All rights reserved.
//

import Foundation
import SWXMLHash
import SwiftSpinner
import CoreData

class dataHelper {
    
    class func getCards(completion: (result: [Card]) -> Void){
        
        var urlstring = String()
        var cards = [Card]()
        
        let moc = DataController().managedObjectContext
        let cardsFetch = NSFetchRequest(entityName: "NewsEntity")
        do {
            let results =
                try moc.executeFetchRequest(cardsFetch)
            for result in results as! [NewsEntity] {
                let card = Card(title: result.title, description: result.descrip, link: result.link, image:result.image)
                cards.append(card)
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        let fetchRequest = NSFetchRequest()
        let entityDescription = NSEntityDescription.entityForName("Config", inManagedObjectContext: DataController().managedObjectContext)
        
        fetchRequest.entity = entityDescription
        
        do {
            let result = try DataController().managedObjectContext.executeFetchRequest(fetchRequest)
            let config = result[0] as! NSManagedObject
            urlstring = (config.valueForKey("url")  as? String)!

        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        
        let request = NSMutableURLRequest(URL: NSURL(string: urlstring)!)
        let session = NSURLSession.sharedSession()
        request.HTTPMethod = "GET"
        
        var err: NSError?
        
        let task = session.dataTaskWithRequest(request) {
            (data, response, error) in
            
            if data == nil {
                print("dataTaskWithRequest error: \(error)")
                SwiftSpinner.show((error?.localizedDescription)!, animated: false).addTapHandler({
                    SwiftSpinner.hide()
                })
                return
            }
            
            func pars(){
                let xml = SWXMLHash.parse(data!)
                for elem in xml["rss"]["channel"]["item"] {
                    let card = Card(title: elem["title"].element?.text, description: elem["description"].element?.text, link: elem["link"].element?.text, image: elem["media:thumbnail"].element!.attributes["url"]!)
                    
                    if cards.indexOf({$0.title == card.title!}) == nil {
                        saveCard(card)
                    }
                    
//                    let index = cards.indexOf {
//                        $0.title == card.title!
//                    }
                }
                completion(result: cards)
            }
            
            func saveCard(cardToSave: Card?) {
                let moc = DataController().managedObjectContext
                let entity = NSEntityDescription.insertNewObjectForEntityForName("NewsEntity", inManagedObjectContext: moc) as! NewsEntity
                
                entity.setValue(cardToSave!.title, forKey: "title")
                entity.setValue(cardToSave!.description, forKey: "descrip")
                entity.setValue(cardToSave!.link, forKey: "link")
                entity.setValue(cardToSave!.image, forKey: "image")
                
                do {
                    try moc.save()
                } catch let error as NSError  {
                    print("Could not save \(error), \(error.userInfo)")
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), pars)
        }
        task.resume()
    }
    
        
}
